


#cmake_minimum_required(VERSION 3.8)
#project(dlib3)

#set(CMAKE_CXX_STANDARD 11)

#set(SOURCE_FILES main.cpp)
#add_executable(dlib3 ${SOURCE_FILES})

cmake_minimum_required(VERSION 3.8)
project(dlib3)
include(../dlib/cmake)
#set(CMAKE_CXX_STANDARD 11)
#set(SOURCE_FILES main.cpp)
#add_executable(dlib1 ${SOURCE_FILES})



# Tell CMake to compile a program.  We do this with the ADD_EXECUTABLE()
# statement which takes the name of the output executable and then a list of
# .cpp files to compile.  Here each example consists of only one .cpp file but
# in general you will make programs that const of many .cpp files.
ADD_EXECUTABLE(main main.cpp)
# Then we tell it to link with dlib.
TARGET_LINK_LIBRARIES(main dlib::dlib)
