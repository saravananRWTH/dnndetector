#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <shlobj.h> 
#include <vector>
#include <iostream>
#include <iostream>
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>
#include <Shlobj.h>
#include <CommDlg.h>

#include "GUI.hpp"
#include "readFolder.hpp"
#include "ROI.hpp"

using namespace cv;
using namespace std;

Mat image;



int main()
{

	int test_or_train;
	cout << "Press 1 for training the image 2 for testing the image \n";
	cin >> test_or_train;

	ReadAndShowImage rasi = ReadAndShowImage();
	string folderName=rasi.GUI();
	//rasi.image_Metadata_Stylesheet(folderName);
	rasi.initiateXMLFile(folderName,test_or_train);
	std::vector <std::string> fileNames = rasi.readFolder(folderName);
	cout << "press 'S' or 's' to save the selection \n";
	cout << "press 'A' or 'a' to cancel the selection \n";
	cout << "press 'N' or 'n' to go to next image \n";
	

	
	for (std::string& fileName : fileNames) {
		cout << fileName;
		ROI *roi=new ROI;
		roi->source(fileName,folderName, test_or_train);
		delete roi;
		
	}
	rasi.endXMLFile(folderName, test_or_train);
	
	return 0;
}

