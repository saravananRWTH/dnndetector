#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "GUI.hpp"
#include <opencv2/opencv.hpp>
#include <shlobj.h>  
#include "readFolder.hpp"
#include <vector>

#include <iostream>
#include <fstream>
#include <windows.h>

#include <tchar.h>
#include <dirent.h>

using namespace cv;
using namespace std;
#define BACKSLASH '\\'

/*============================================================================*/
/* NAME:GUI																	 */
/*============================================================================*/



std::string ReadAndShowImage::GUI()
{
	TCHAR folderPath[MAX_PATH] = { 0 }; //path of the folder
	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(BROWSEINFO));
	bi.hwndOwner = NULL;
	bi.pszDisplayName = folderPath;
	bi.lpszTitle = "Selected folder directory from below:";
	bi.ulFlags = BIF_RETURNFSANCESTORS;
	LPITEMIDLIST idl = SHBrowseForFolder(&bi);
	SHGetPathFromIDList(idl, folderPath); //get the path of the folder and store in the variable
	std::vector <std::string> fileNames; //store the list of fileNames
	cv::Mat image;

	if (NULL != idl)
	{
		std::cout << "selected folder is" << folderPath << std::endl;
		return string(folderPath);

	}

	else
	{
		// All this stuff below is to tell you exactly how you messed up above. 
		// Once you've got that fixed, you can often (not always!) reduce it to a 'user cancelled' assumption.
		switch (CommDlgExtendedError())
		{
		case CDERR_DIALOGFAILURE: std::cout << "CDERR_DIALOGFAILURE\n";   break;
		case CDERR_FINDRESFAILURE: std::cout << "CDERR_FINDRESFAILURE\n";  break;
		case CDERR_INITIALIZATION: std::cout << "CDERR_INITIALIZATION\n";  break;
		case CDERR_LOADRESFAILURE: std::cout << "CDERR_LOADRESFAILURE\n";  break;
		case CDERR_LOADSTRFAILURE: std::cout << "CDERR_LOADSTRFAILURE\n";  break;
		case CDERR_LOCKRESFAILURE: std::cout << "CDERR_LOCKRESFAILURE\n";  break;
		case CDERR_MEMALLOCFAILURE: std::cout << "CDERR_MEMALLOCFAILURE\n"; break;
		case CDERR_MEMLOCKFAILURE: std::cout << "CDERR_MEMLOCKFAILURE\n";  break;
		case CDERR_NOHINSTANCE: std::cout << "CDERR_NOHINSTANCE\n";     break;
		case CDERR_NOHOOK: std::cout << "CDERR_NOHOOK\n";          break;
		case CDERR_NOTEMPLATE: std::cout << "CDERR_NOTEMPLATE\n";      break;
		case CDERR_STRUCTSIZE: std::cout << "CDERR_STRUCTSIZE\n";      break;
		case FNERR_BUFFERTOOSMALL: std::cout << "FNERR_BUFFERTOOSMALL\n";  break;
		case FNERR_INVALIDFILENAME: std::cout << "FNERR_INVALIDFILENAME\n"; break;
		case FNERR_SUBCLASSFAILURE: std::cout << "FNERR_SUBCLASSFAILURE\n"; break;
		default: std::cout << "You cancelled.\n";
		}
	}


}
/*============================================================================*/
/* NAME:READFOLDER															 */
/*============================================================================*/

std::vector <std::string> ReadAndShowImage::readFolder(string folderPath)
{

		TCHAR* folderPath1 = new TCHAR(TCHAR(folderPath.size() + 1));
		folderPath1[folderPath.size()] = 0;
		std::copy(folderPath.begin(),folderPath.end(),folderPath1); //converting string to TCHAR

		struct dirent *entry;
		DIR *dp;
		string window_name = "training set";
		std::vector <std::string> fileNames;
		


		dp = opendir(folderPath1);
		if (dp == NULL) {
			perror("opendir: Path does not exist or could not be read.");
			}

		while ((entry = readdir(dp)))
		{
			Mat image;

			//appending fileName with the path of the folder
			string fileName = string(folderPath)+BACKSLASH+string(entry->d_name);
			image = imread(fileName); // Read the file
			//std::cout << fileName<<std::endl;
			
			if (!image.data) // Check for invalid input
			{
				std::cout << "Could not open or find the image" << std::endl;
				
				
			}
			else
			{
				fileNames.push_back(fileName);
				
				//imshow(window_name, image);//display the image
				//int c = waitKey(100000000000000);//waiting time in milliseconds
			
			}
			fileName.clear();

		}
		
		closedir(dp);

		return fileNames;
		
	
}

/*============================================================================*/
/* NAME:INITIATEXMLFILE															 */
/*============================================================================*/

void ReadAndShowImage::initiateXMLFile(string folderPath, int test_or_train)
{
	//cout << "checking for xml file \n";
	//cout << folderPath.append("\\training.xml")<< "\n";
	string training = folderPath;
	string testing = folderPath;

	if (test_or_train==1)
	{
		ofstream createTrainXML(training.append("\\training.xml"));
		createTrainXML << "<?xml version='1.0' encoding='ISO-8859-1'?> \n";
		createTrainXML << "<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>";
		createTrainXML << "\n <dataset> \n <name>Training faces</name> \n <comment>These are images from the PASCAL VOC 2011 dataset.</comment>\n";
		createTrainXML << "<images>";
		createTrainXML.close();

	}
		
		
	else
	{
	
		ofstream createTestXML(testing.append("\\testing.xml"));
		createTestXML << "<?xml version='1.0' encoding='ISO-8859-1'?> \n";
		createTestXML << "<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>";
		createTestXML << "\n <dataset> \n <name>Testing faces</name> \n <comment>These are images from the PASCAL VOC 2011 dataset.</comment>\n";
		createTestXML << "<images>";
		createTestXML.close();
	}
}

/*============================================================================*/
/* NAME:ENDXMLFILE															 */
/*============================================================================*/

void ReadAndShowImage::endXMLFile(string folderPath,  int test_or_train)
{
	string training = folderPath;
	string testing = folderPath;

	if (test_or_train == 1)
	{
		ofstream endTrainXML(training.append("\\training.xml"), ios::app);

		endTrainXML << "\n </images> \n</dataset>";
		endTrainXML.close();
	}
	else
	{

	ofstream endTestXML(testing.append("\\testing.xml"),ios::app);
	endTestXML << "\n </images> \n</dataset>";
	
	endTestXML.close();
	}

}

/*============================================================================*/
/* NAME:IMAGEMETADATASTYLESHEET															 */
/*============================================================================*/

void ReadAndShowImage::image_Metadata_Stylesheet(string folderPath)
{
	ofstream  metaData(folderPath.append("\\image_metadata_stylesheet.xslt"), ios::app);

	metaData << "<?xml version=\"1.0\" encoding=\"ISO - 8859 - 1\"?>" <<"\n";
	metaData << "<xsl:stylesheet version = \"1.0\" xmlns:xsl =\"http://www.w3.org/1999/XSL/Transform\">" << "\n";
	metaData << "<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>" << "\n";
	metaData << "<!--************************************************************************* -->"<<"\n\n";
	metaData << "\t <xsl:variable name=\"max_images_displayed\">30</xsl:variable>"<<"\n\n";
	metaData << "<!-- ************************************************************************* -->"<<"\n\n";

	metaData << "\t<xsl:template match=\"/dataset\">";
	metaData << "\t\t<html>" << "\n" << "\t\t <head>"<<"\n"<<"\n";
	metaData << "\t\t\t <style type=\"text/css\">"<<"\n";
	metaData << "\t\t\t\tdiv#box{" << "\n" << "\t\t\t\t position: absolute;" << "\n" << "\t\t\t\t border-style:solid;" << "\n" << "\t\t\t\t border-width:1px;" << "\n" << "\t\t\t\t  border-color:red;\n}"<<"\n";
	metaData << "div#circle{" << "\n" << "position: absolute;" << "\n" << "border-style:solid;" << "\n" << "border-width:1px;" << "\n" << " border-color:red;" << "\n";
	metaData<< "border-radius_7px;" << "\n" << "width:1px;" << "height:1px;}" << "\n";
	metaData << "div#labe{" << "\n" << "position: absolute;" << "color:red;}" << "\n";
	metaData << "div#img{" << "\n" << "position: relative;" << "margin-bottom: 2em;}" << "\n";
	metaData << "pre{" << "\n" << "color: black;" << "margin: 1em 0.25in;" << "\n" << "padding: 0.5em;" << "\n" << " background: rgb(240,240,240);";
	metaData << "\n" << "border - top: black dotted 1px;" << "\n";
    metaData << "border - left: black dotted 1px;" << "\n" << "border - right: black solid 2px;" << "\n" << "border - bottom: black solid 2px;}"<<"\n";
	metaData << "\n"<<"</style>"<<"\n"<<"</head>"<<"\n"<<"";
	metaData << "<body>"<<"\n";
	metaData << "Dataset name: <b><xsl:value-of select='/dataset/name'/></b> <br/>"<<"\n";
	metaData << "Dataset comment: <pre><xsl:value-of select='/dataset/comment'/></pre> <br/> " << "\n";
	metaData << " Number of images: <xsl:value-of select=\"count(images / image)\"/> <br/>" << "\n";
	metaData << " Number of boxes: <xsl:value-of select=\"count(images / image / box)\"/> <br/>"<<"\n";
	metaData << "<br/>" << "<hr/>" << "\n";
	metaData << " <!-- Show a warning if we aren't going to show all the images -->"<<"\n";
	metaData << " <xsl:if test=\"count(images / image) &gt; $max_images_displayed\">" << "\n";
	metaData << " <h2>Only displaying the first <xsl:value-of select=\"$max_images_displayed\"/> images.</h2>"<<"\n";
	metaData << "<hr/>"<<"\n"<<"< / xsl:if>"<<"\n";
	metaData << "<xsl:for-each select=\"images / image\">" <<"\n" ;
	metaData << "<!-- Don't try to display too many images.  It makes your browser hang -->"<<"\n";
	metaData << "<xsl:if test=\"position(); = $max_images_displayed\">";
	metaData << "<b><xsl:value-of select=\"@file\"/></b> (Number of boxes : <xsl : value - of select = \"count(box)\"/>)" << "\n";
	metaData << "<div id=\"img\">"<<"\n";
	metaData << " <img src=\"{@file}\"/>";
	metaData << "<xsl:for-each select=\"box\">"<< "\n";
	metaData <<"<div id=\"box\" style=\"top: {@top}px; left: {@left}px; width: {@width}px; height: {@height}px;\"></div>" <<"\n";
	metaData << "<!-- If there is a label then display it in the lower right corner. -->"<<"\n";
	metaData << "<xsl:if test=\"label\">" << "\n";
	metaData << " <div id=\"label\" style=\"top: {@top+@height}px; left: {@left+@width}px;\">" << "\n";
	metaData <<"<xsl:value - of select = \"label\" / >"<<"\n";
	metaData << "< / div>" << "\n";
	metaData << "< / xsl:if>" << "\n";
	metaData << "<xsl:for - each select = \"part\">"<<"\n";
	metaData << "<div id=\"circle\" style=\"top: {(@y)}px; left: {(@x)}px;\"></div>"<<"\n";
	metaData << "</xsl:for-each>"<<"\n";
	metaData << "</xsl:for-each>" << "\n";
	metaData << "</div>" << "\n";
	metaData << "</xsl:if>" << "\n";
	metaData << "</xsl:for-each>"<<"\n";
	metaData << "</body>"<<"\n";
	metaData << "</html>" << "\n";
	metaData << "</ xsl:template>" << "\n";
	metaData << "<!-- ************************************************************************* -->"<<"\n";
	metaData << "</xsl:stylesheet>";



	metaData.close();


}

