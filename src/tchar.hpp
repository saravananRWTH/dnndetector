////////////////////////////////////////////////////////////////////////
//                                                                    //
// FileVerifier: A tool for calculating hashes using                  //
//               on files using various different                     //
//               algorithms.                                          //
//                                                                    //
// (C) 2008 Tom Bramer                                                //
//                                                                    //
// Filename: tchar.hpp                                                //
// Description: Function mapping for non-windows systems.             //
// Author: Tom Bramer                                                 //
//                                                                    //
// This program is free software; you can redistribute it and/or      //
// modify it under the terms of the GNU General Public License        //
// as published by the Free Software Foundation; either version 2     //
// of the License, or (at your option) any later version.             //
//                                                                    //
// This program is distributed in the hope that it will be useful,    //
// but WITHOUT ANY WARRANTY; without even the implied warranty of     //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      //
// GNU General Public License for more details.                       //
//                                                                    //
// You should have received a copy of the GNU General Public License  //
// along with this program; if not, write to the Free Software        //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA      //
// 02110-1301, USA.                                                   //
//                                                                    //
////////////////////////////////////////////////////////////////////////

#ifndef __FV_TCHAR_HPP__
#define __FV_TCHAR_HPP__

#include <fv/types.hpp>
#include <fv/defs.hpp>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef _UNICODE

#include <wchar.h>

#ifndef __WIN32__
#define _tcstoul wcstoul
#define _ttoi wtoi
#endif

#else

#ifndef __WIN32__
#define _tcstoul strtoul
#define _ttoi atoi
#endif

#endif

#ifdef TCHAR_USE_TOSTREAM
#include <iostream>
#ifdef _UNICODE
static std::wostream& tcout = std::wcout;
static std::wistream& tcin = std::wcin;
static std::wostream& tcerr = std::wcerr;
#else
static std::ostream& tcout = std::cout;
static std::istream& tcin = std::cin;
static std::ostream& tcerr = std::cerr;
#endif
#endif

#endif /* __FV_TCHAR_HPP__ */

