#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/opencv.hpp>
#include <shlobj.h>  
#include <vector>


#include <iostream>
#include <windows.h>
#include <tchar.h>
#include <dirent.h>


#include "ROI.hpp"
#include "GUI.hpp"
#include "readFolder.hpp"

using namespace std;
using namespace cv;
Rect box;
bool isRectDrawn=false;
bool drawing_box = false;
char win_name[]="Image";
static int index = 0;
static int counter = 0;
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/


ROI::ROI() {
	
	
}
ROI::~ROI() {


}

/*============================================================================*/
/* NAME:DRAW BOX                                                  */
/*============================================================================*/


void ROI::draw_box( IplImage* img, CvRect rect ) {
    cvRectangle (
            img,
            cvPoint(box.x,box.y),
            cvPoint(box.x+box.width,box.y+box.height),
            cvScalar(0x00,0x00,0xff) /* blue */
    );
}

/*============================================================================*/
/* NAME:DRAW_BOX_GREEN														  */
/*============================================================================*/

void ROI::draw_box_green( IplImage* img, CvRect rect ) {
    cvRectangle (
            img,
            cvPoint(box.x,box.y),
            cvPoint(box.x+box.width,box.y+box.height),
            cvScalar(0x00,0xff,0x00) /* green */
    );
}

/*============================================================================*/
/* NAME:WRITE_XML_FILE														  */
/*============================================================================*/

void ROI::writeXML(string folderName,string fileName,CvRect box,int test_or_train)
{
	int height = box.height;
	int width = box.width;
	//to maintain the aspect ratio 4:3 of the bounding box
	if (round(width / height) != 2)
	{
		width = round((4 / 3)*height);
	}
	if (test_or_train == 1) {
	ofstream writeTrainXML(folderName.append("\\training.xml"), ios::app);
	
	static string line;
	counter += 1;
	if(counter==1)
	{
		writeTrainXML << "\n <image file = " << "'" << fileName << "'>";
	}
		writeTrainXML << "\n<box top='" << box.y << "' left='" << box.x<<"' width='"<<width<<"' height='"<<height<<"'/>\n";
	
		

		writeTrainXML.close();
	}
	else if (test_or_train == 2)
	{
		ofstream writeTestXML(folderName.append("\\testing.xml"), ios::app);

		static string line;
		counter += 1;
		if (counter == 1)
		{
			writeTestXML << "\n <image file = " << "'" << fileName << "'>";
		}
		writeTestXML << "\n<box top='" << box.y << "' left='" << box.x << "' width='" << width << "' height='" << height << "'/>\n";



		writeTestXML.close();
	}
		

}

// This is our mouse callback. If the user
// presses the left button, we start a box.
// when the user releases that button, then we
// add the box to the current image_copy. When the
// mouse is dragged (with the button down) we
// resize the box.
//
/*============================================================================*/
/* NAME:MY_MOUSE_CALLBACK													  */
/*============================================================================*/

 void ROI::my_mouse_callback(
	int event, int x, int y,int flag,void* param) {
	IplImage* image_copy = (IplImage*)param;
	switch (event) {
	case CV_EVENT_MOUSEMOVE: {
		if (drawing_box) {
			box.width = x - box.x;
			box.height = y - box.y;
		}
	}
							 break;
	case CV_EVENT_LBUTTONDOWN: {
		drawing_box = true;
		box = cvRect(x, y, 0, 0);
	}
							   break;
	case CV_EVENT_LBUTTONUP: {
		drawing_box = false;
		isRectDrawn = true;
		if (box.width<0) {
			box.x += box.width;
			box.width *= -1;
		}
		if (box.height<0) {
			box.y += box.height;
			box.height *= -1;
		}
		draw_box(image_copy, box);
	}
							 break;
	}
}

 /*============================================================================*/
 /* NAME:SOURCE	  															 */
 /*============================================================================*/


 int ROI::source(std::string fileName,std::string folderName, int test_or_train) {
	
		
		//TCHAR* folderPath1 = new TCHAR(TCHAR(fileName.size() + 1));
		//folderPath1[fileName.size()] = 0;
		//std::copy(fileName.begin(), fileName.end(), folderPath1); //converting string to TCHAR
	  box = cvRect(-1, -1, 0, 0);
	 const char *folderPath1 = fileName.c_str();
	   char *y = new char[fileName.length() + 1]; // or
										   // char y[100];

	   std::strcpy(y, fileName.c_str());
	   cout << y;
	   
		IplImage* image_input = cvLoadImage(y);
		delete[] y;
		IplImage* image = cvCloneImage(image_input);
		IplImage* image_copy = cvCloneImage(image);
		IplImage* temp = cvCloneImage(image_copy);
	   
		

		cvNamedWindow("Image");
		
		// Here is the crucial moment that we actually install
		// the callback. Note that we set the value ‘param’ to
		// be the image_copy we are working with so that the callback
		// will have the image_copy to edit.
		//
		
			setMouseCallback(
			"Image",
			my_mouse_callback,
			(void*)image_copy
		);
		

		// The main program loop. Here we copy the working image_copy
		// to the ‘temp’ image_copy, and if the user is drawing, then
		// put the currently contemplated box onto that temp image_copy.
		// display the temp image_copy, and wait 15ms for a keystroke,
		// then repeat…
		//

		try
		{

			
			while (1)
	 {
			//cvCopyImage( image_copy, temp );
			cvCopy(image_copy, temp);
			if (drawing_box) draw_box(temp, box);
			cvShowImage("Image", temp);
		
			//if( cvWaitKey( 15 )==27 ) break;
			int key1 = cvWaitKey(1000000000000000);
			if (key1 == 27) break;
			//if (key == 'n' || key == 'N') break;
			if (isRectDrawn) {
				if (key1 == 's' || key1 == 'S') {
					// draw green box
					draw_box_green(image_copy, box);
					cvCopy(image_copy, image);

					// save roi image

					char save_image_name[128] = {0};
					sprintf(save_image_name, "E:\\VR_Lab\\dnndetector\\training_set\\rect_%d.jpg", index++);
					cvSetImageROI(image_input, box);
					writeXML(folderName, fileName,box,test_or_train);
					cvShowImage("crop", image_input);
					try
					{

					
					cvSaveImage(save_image_name, image_input);
					}
					catch (const std::exception& e)
					{
						std::cout << e.what();
					}
				  
				  cvResetImageROI(image_input);
					isRectDrawn = false;
				}

				if (key1 == 'q' || key1 == 'Q') {
					cvCopy(image, image_copy);
					isRectDrawn = false;
				}

				if (key1 == 'N' || key1 == 'n')
				{
					counter = 0;
					if (test_or_train == 1) {
					ofstream writeTrainXML(folderName.append("\\training.xml"), ios::app);
					writeTrainXML << "</image>\n";
					writeTrainXML.close();
					}
					else if (test_or_train == 2) {
						ofstream writeTestXML(folderName.append("\\testing.xml"), ios::app);
						writeTestXML << "</image>\n";
						writeTestXML.close();
					}

					break;
				}
				
			}
		}
		}
		catch (const std::exception& e)
		{
			std::cout << e.what();
		}

		// Be tidy
		//
		cvReleaseImage(&image_copy);
		cvReleaseImage(&temp);
		//cvReleaseImage(&image);
		//cvReleaseImage(&image_input);
		cvDestroyWindow("Image");
	

	return 0;
}

