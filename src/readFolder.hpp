#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "GUI.hpp"
#include <opencv2/opencv.hpp>
#include <shlobj.h>  


#include <iostream>
#include <filesystem>
#include <dirent.h>
#include <vector>
#ifndef READANDSHOWIMAGE_H
#define READANDSHOWIMAGE_H

class ReadAndShowImage
{

public:

	/*
	*open the GUI for the file dialog
	*/
	std::string  GUI();


	/* browsing the folder and reading the image inside the folder
	 *@param folderPath - path of the folder seleccted from the GUI
	 */
	std::vector <std::string> readFolder(string folderPath); //store the list of fileNames
	
	/* initiate the XML File and create the initial headers
	 *@param folderPath - path of the folder seleccted from the GUI
	*/

	
	void initiateXMLFile(string folderPath,int test_or_train);
	
	/* end the XML File and create the initial headers
	*@param folderPath - path of the folder seleccted from the GUI
	*/


	void endXMLFile(string folderPath, int test_or_train);

	/* create metadata style sheet for the training and testing set
	*@param folderPath - path of the folder seleccted from the GUI
	*/
	void image_Metadata_Stylesheet(string folderPath);


};

#endif // READANDSHOWIMAGE_H