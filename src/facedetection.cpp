#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "facedetection.hpp"

using namespace cv;
using namespace std;


/** Global variables */
String face_cascade_name = "E:\\VR_Lab\\opencv\\data\\haarcascades\\haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "E:\\VR_Lab\\opencv\\data\\haarcascades\\haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);

/* int main(int argc, char** argv)
{
	int cvCreateTrackbar(const char* trackbar_name, const char* window_name, int* value, int count, CvTrackbarCallback on_change = NULL)


	{
		cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
		return -1;
	}

	Mat image;
	image = imread(argv[1]); // Read the file


	if (!image.data) // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	//namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	face_cascade.load(face_cascade_name);
	if (!face_cascade.load(face_cascade_name))
	{
		printf("--(!)Error loading\n");

	}

	if (!eyes_cascade.load(eyes_cascade_name)) {
		printf("--(!)Error loading eyes cascade\n");
		return -1;
	}


	if (!image.empty()) {
		detectAndDisplay(image);
	}
	else {
		cout << " --(!) No captured frame -- Break!";

	}
	imshow(window_name, image);

	int c = waitKey(100000000000000);

	if (27 == char(c)) {

	}
	
	
	return 0;
}*/


void detectAndDisplay(Mat image)
{
	std::vector<Rect> faces;
	Mat frame_gray;

	cvtColor(image, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);
	

	faces.clear();
	//-- Detect faces
	try
	{
		face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
		// call OpenCV
	}
	catch (cv::Exception& e)
	{
		const char* err_msg = e.what();
		std::cout << "exception caught: " << err_msg << std::endl;
	}

	for (size_t i = 0; i < faces.size(); i++)
	{
		//Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
		//ellipse(image, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
		
		Rect rect(faces[i].x, faces[i].y, faces[i].height, faces[i].width);
		rectangle(image, rect, Scalar(255, 0, 255));


		Mat faceROI = frame_gray(faces[i]);
		std::vector<Rect> eyes;

		//-- In each face, detect eyes
		eyes_cascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

		for (size_t j = 0; j < eyes.size(); j++)
		{
			
			Rect rect1(eyes[j].x, eyes[j].y, eyes[j].height, eyes[j].width);
			rectangle(image, rect1, Scalar(255, 0, 255));
			
		}
	}
	//-- Show what you got
	imshow(window_name, image);
}