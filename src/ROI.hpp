//
// Created by Jia on 17/5/29.
//
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "GUI.hpp"
#include <opencv2/opencv.hpp>
#include <shlobj.h>  
#include "readFolder.hpp"
#include <vector>



#include <iostream>

#include <windows.h>

#include <tchar.h>
#include <dirent.h>

#ifndef ROI_H
#define ROI_H


class ROI {
	
private:
	
	/* draw box function
	*@param img Image
	*@param rect box to be draw on the image

	*/
	static void draw_box(IplImage* img, CvRect rect);
	
	
	
	/* draw box in green colour
	*@param img Image
	*@param rect box to be draw on the image

	*/
	static void draw_box_green(IplImage* img, CvRect rect);

	
	/* handles mouse events
	*@param event- MOUSELBUTTONUP,MOUSELBUTTONDOWN
	*@param x  x coordinates
	*@param y y coordinate
	*@param flag
	*@param param
	*/
	static void my_mouse_callback(
		int event, int x, int y, int flags, void* param
	);


	/*write into XML Files
	*/
	static void writeXML(string folderName,string fileName, CvRect box, int test_or_train);

public:
	
	//constructor. initialize the objects

	ROI();
	~ROI();



	/* source function to draw rectangle
	* @param fileNames- List of fileNames
	@param folderName- to write the components into XML file
	*/
	int source(std::string fileName,std::string folderName, int test_or_train);
	

};
#endif //ROI_H
