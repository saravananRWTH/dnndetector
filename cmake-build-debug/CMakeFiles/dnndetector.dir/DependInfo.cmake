# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Jia/dnndetector/src/GUI.cpp" "/Users/Jia/dnndetector/cmake-build-debug/CMakeFiles/dnndetector.dir/src/GUI.cpp.o"
  "/Users/Jia/dnndetector/src/facedetection.cpp" "/Users/Jia/dnndetector/cmake-build-debug/CMakeFiles/dnndetector.dir/src/facedetection.cpp.o"
  "/Users/Jia/dnndetector/src/readFolder.cpp" "/Users/Jia/dnndetector/cmake-build-debug/CMakeFiles/dnndetector.dir/src/readFolder.cpp.o"
  "/Users/Jia/dnndetector/src/regionOfInterest.cpp" "/Users/Jia/dnndetector/cmake-build-debug/CMakeFiles/dnndetector.dir/src/regionOfInterest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include/*.hpp"
  "ext/gtest/src/googletest/googletest/include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
